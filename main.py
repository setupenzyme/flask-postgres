from server.database import app
from server.routes import router

app.register_blueprint(router)

@app.get('/health')
def health():
  return {'health': "good"}

if __name__ == "__main__":
    app.run(debug=True)